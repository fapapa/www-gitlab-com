---
layout: markdown_page
title: "CXC"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

The following benefits are provided by [CXC](https://cxcglobal.com/) and apply to team members who are contracted through CXC. If there are any questions, these should be directed to People Operations at GitLab who will then contact the appropriate individual at CXC.

## Australia

- CXC does not provide private healthcare
- Contributions are made into [Superannuation](https://www.ato.gov.au/Individuals/Super/). The current contribution is 9.5% and will increase or decrease according to Australian law. To check your Super you can login online at the [MyGov website](https://my.gov.au/LoginServices/main/login?execution=e1s1)

## Canada

- CXC does not provide private healthcare
  * GitLab adds an additional 300 USD per month in a medical allowance to the contract with CXC to provide comprehensive health coverage since the public healthcare only provides coverage for medically necessary health care services. 
- Individuals will receive 4-6% vacation pay depending on the province where applicable
- Employer contributions are made to the federal Canada Pension Plan (CPP)
- Employees looking for more information can review the [CXC for Canada document](https://docs.google.com/document/d/1Pf1wffqa58A_dzoiA3_Ta7ccXtMcsoKhROoQM4zjwHg/edit?usp=sharing) (internal).
