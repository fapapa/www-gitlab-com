---
layout: markdown_page
title: "Category Strategy - Live Coding"
---

- TOC
{:toc}

## Live Coding

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting this category strategy page on Live Coding in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by Kai Armstrong([E-Mail](mailto:karmstrong@gitlab.com)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=live%20coding) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=live%20coding) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for Live Coding, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

Live shared programming environments allow pair programming and collaboration between developers in different locations with greater ease.

<!-- ### Where we are Headed -->
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

<!-- ### Target Audience and Experience -->
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

**Next: [Broadcast Web IDE](https://gitlab.com/groups/gitlab-org/-/epics/694)**

Read only broadcasting from the Web IDE will allow the author of a merge request to talk another developer through a change using the Web IDE (audio via Zoom/Hangouts), and allow the reviewer to jump to other parts of the code if they want to dig deeper.

Broadcasting should be a simpler problem because changes are only flowing in a single direction, removing the problem of conflicts. However, using operational transformations, we should be able to quickly follow up with a second iteration that allows multiple people to edit simultaneously.

There are also other interesting applications of broadcasting, like in the classroom allowing everyone to follow along on a lecture/demo taught from the editor.

<!-- ### What is Not Planned Right Now -->
<!-- Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

<!-- ### Maturity Plan -->
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [Microsoft Live Share](https://www.visualstudio.com/services/live-share/)
- [Codesandbox.io](https://codesandbox.io/docs/live) – [Announcement](https://hackernoon.com/introducing-codesandbox-live-real-time-code-collaboration-in-the-browser-6d508cfc70c9)


<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- ### Top user issue(s) -->
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!-- ### Top internal customer issue(s) -->
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

<!-- ### Top Strategy Item(s) -->
<!-- What's the most important thing to move your strategy forward?-->

